﻿// See https://aka.ms/new-console-template for more information
//Console.WriteLine("Hello, World!");

using NDownloadManagerLib.Base;
using NDownloadManagerLib.Base.Net;

public static class NDownloadManagerConsole
{
    /// <summary>
    /// Download the URL specified and save the contents to filename..
    /// </summary>
    /// <param name="url">url</param>
    /// <param name="filename">filename</param>
    private static void Download(string url, string filename)
    {
        NDownloadManagerLib.Base.IO.File file = new NDownloadManagerLib.Base.IO.File()
        {
            Url = url
        };

        file.GetFileSize(url);
        //DownloadResource.Download(url, filename);
    }

    private static void CommandHandler(string input)
    {
        // include array later to have command, args, delegate, description

        if (string.IsNullOrEmpty(input)) { return; }
        if (input.StartsWith(@"Help", StringComparison.OrdinalIgnoreCase))
        {
            Console.WriteLine("... Help ...");
            Console.WriteLine("Download <URL>");
            return;
        }

        if (input.StartsWith(@"Download", StringComparison.InvariantCultureIgnoreCase))
        {
            var url = input.Substring(@"Download ".Length);

            Download(url.Trim(), "");
        }

        else if (input.StartsWith(@"ShowProgress", StringComparison.InvariantCultureIgnoreCase))
        {
            for (int i = 0; i <= 100; i++)
            {
                ConsoleProgress.ShowProgress(i, 100);

                Thread.Sleep(10);
            }

        }
    }

    public static void Main(string[] args)
    {
        bool exit = false;
        string command = string.Empty;
        while (!exit)
        {
            Console.Write(Environment.NewLine + "> ");

            command = Console.ReadLine();
            CommandHandler(command);
            if (command.Equals(@"exit", StringComparison.InvariantCultureIgnoreCase))
                exit = true;
        }

        return;
    }

};