﻿using NDownloadManagerLib.Base.IO;
using NDownloadManagerLib.Base.Net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace NDownloadManagerLib
{
    [Serializable]
    [DataContract]
    public class Configuration : ISerializable
    {
        SpinLock _spinLock = new SpinLock();
        bool _lockTaken = false;

        public static Configuration ConfigurationInstance = new Configuration();
        private UrlConfigurationCollection _urlConfigurations = new UrlConfigurationCollection();

        [DataMember]
        public UrlConfigurationCollection UrlConfigurations 
        { 
            get {
                UrlConfigurationCollection _urlconfigurations;
                _lockTaken = false;
                _spinLock.Enter(ref _lockTaken);

                _urlconfigurations = _urlConfigurations;

                _spinLock.Exit();

                return _urlconfigurations;
            }

            set
            {
                _lockTaken = false; 
                _spinLock.Enter(ref _lockTaken);

                _urlConfigurations = value;

                _spinLock.Exit();
            }
        }

        private int nThreadsPerFile = 10;
        private readonly object _lockNThreadValue = new object();

        [DataMember]
        public int NumberOfThreadsPerFile {
            get
            {
                return nThreadsPerFile; 
            }
            set 
            {
                lock (_lockNThreadValue)
                {
                    if (value > 10) nThreadsPerFile = 10;
                    else if (value > 0) nThreadsPerFile = value;
                }
            } 
        }

        private int nThreadsPerProject = 10;

        [DataMember]
        public int NumberOfThreadsPerProject 
        { 
            get 
            { 
                return nThreadsPerProject;
            } 
            set 
            {
                lock (_lockNThreadValue)
                {
                    if (nThreadsPerProject > 256)
                    {
                        nThreadsPerProject = 256;
                    }
                    else if (nThreadsPerProject > 0)
                    {
                        nThreadsPerProject = value;
                    }
                }
            } 
        }

        public Configuration() { }

        private UrlConfiguration[] _urlConfigurationItems = null;

        public Configuration(SerializationInfo info, StreamingContext context) 
        {
            _urlConfigurationItems = (UrlConfiguration[])info.GetValue("UrlConfigurations", typeof(UrlConfiguration[]));
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            _lockTaken = false;
            _spinLock.Enter(ref _lockTaken);

            if (_urlConfigurationItems != null)
            {
                foreach (var item in _urlConfigurationItems)
                {
                    UrlConfigurations.Add(item);
                }
            }

            _urlConfigurationItems = null;

            _spinLock.Exit();
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            List<UrlConfiguration> urlConfigurations = new List<UrlConfiguration>();

            _lockTaken = false;
            _spinLock.Enter(ref _lockTaken);

            while (UrlConfigurations.Count > 0)
            {
                urlConfigurations.Add(UrlConfigurations.Take());
            }

            info.AddValue("UrlConfigurations", urlConfigurations);

            foreach (var item in urlConfigurations)
            {
                UrlConfigurations.Add(item);
            }

            _spinLock.Exit();
        }

        public void Save(string filename = @"NDownloadManager.conf")
        {
            try
            {
                using (var fs = System.IO.File.OpenWrite(filename))
                {
                    Serialize(fs);
                    fs.Close();
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine(e);
#endif
                throw new FileSaveException("Unable to save to the configuration file " + filename, filename);
            }
        }

        public void Load(string filename = @"NDownloadManager.conf")
        {
            try
            {
                using (var fs = System.IO.File.OpenRead(filename))
                {
                    DeSerialize(fs);
                    fs.Close();
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine(e);
#endif
                throw new FileLoadException("Configuration Load Error, Try deleting the file " + Path.GetFullPath(filename), filename);
            }
        }

        internal void Serialize(Stream stream)
        {
            _lockTaken = false;
            _spinLock.Enter(ref _lockTaken);
            
            List<UrlConfiguration> _urlConfigurations = new List<UrlConfiguration>();
            while (this._urlConfigurations.Count > 0)
                _urlConfigurations.Add(this._urlConfigurations.Take());

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings()
            {
                OmitXmlDeclaration = true
            };
            XmlWriter xmlWriter = XmlWriter.Create(stream);
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("Configuration");
            
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<UrlConfiguration>));
            xmlSerializer.Serialize(xmlWriter, _urlConfigurations);

            xmlWriter.WriteStartElement("NumberOfThreadsPerFile");
            xmlWriter.WriteValue(NumberOfThreadsPerFile);
            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("NumberOfThreadsPerProject");
            xmlWriter.WriteValue(NumberOfThreadsPerProject);
            xmlWriter.WriteEndElement();

            xmlWriter.WriteEndDocument();

            xmlWriter.Close();

            foreach (var item in  _urlConfigurations)
                this._urlConfigurations.Add(item);

            _spinLock.Exit();
        }

        /// <summary>
        /// DeSerialize method
        /// deserializes the Configuration object from a Stream which maybe a FileStream..
        /// </summary>
        /// <param name="stream"><see cref="Stream"/> which maybe a <see cref="FileStream"/> or <see cref="MemoryStream"/></param>
        /// <exception cref="Exception">Xml Document Error</exception>
        internal void DeSerialize(Stream stream)
        {
            _lockTaken = false;
            _spinLock.Enter(ref _lockTaken);

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings()
            {
                Async = true
            };

            XmlReader xmlReader = XmlReader.Create(stream, xmlReaderSettings);

            if (!xmlReader.Read() || xmlReader.NodeType != XmlNodeType.XmlDeclaration)
                throw new Exception("XML Document Error", null);

            if (!xmlReader.Read() || xmlReader.NodeType != XmlNodeType.Element || !xmlReader.Name.Equals("Configuration"))
                throw new Exception("XML Document Error", null);

            xmlReader.Read();
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<UrlConfiguration>));
            var _urlConfigurations = xmlSerializer.Deserialize(xmlReader) as List<UrlConfiguration>;

            if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name.Equals("NumberOfThreadsPerFile"))
            {
                if (xmlReader.Read() && xmlReader.NodeType == XmlNodeType.Text)
                   int.TryParse(xmlReader.Value, out this.nThreadsPerFile);

                if (xmlReader.Read() && xmlReader.NodeType == XmlNodeType.EndElement && xmlReader.Name.Equals("NumberOfThreadsPerFile"))
                    ;
                else
                    throw new Exception("XML Document Error", null);
            }
            else
                throw new Exception("XML Document Error", null);

            if (xmlReader.Read() && xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name.Equals("NumberOfThreadsPerProject"))
            {

                if (xmlReader.Read() && xmlReader.NodeType == XmlNodeType.Text)
                    int.TryParse(xmlReader.Value, out this.nThreadsPerProject);

                if (xmlReader.Read() && xmlReader.NodeType == XmlNodeType.EndElement && xmlReader.Name.Equals("NumberOfThreadsPerProject"))
                    ;
                else
                    throw new Exception("XML Document Error", null);
            }
            else
                throw new Exception("XML Document Error", null);

            foreach (var item in _urlConfigurations)
                this._urlConfigurations.Add(item);

            _urlConfigurations.Clear();

            xmlReader.Close();

            _spinLock.Exit();
        }
    }
}
