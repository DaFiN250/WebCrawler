﻿using System;
using NDownloadManagerLib;
using NDownloadManagerLib.Base.Enums;
using NDownloadManagerLib.Base.Interfaces;

namespace NDownloadManagerLib.Base
{
    /// <summary>
    /// Class InRequest.
    /// </summary>
    /// <seealso cref="Request" />
    public class InRequest : Request
    {
        /// <summary>
        /// Gets a value indicating whether this instance is valid request.
        /// </summary>
        /// <value><c>true</c> if this instance is valid request; otherwise, <c>false</c>.</value>
        public bool IsValidRequest => RequestNum != InvalidRequestNum;

        /// <summary>
        /// Initializes a new instance of the <see cref="InRequest"/> class.
        /// </summary>
        /// <param name="requestType">Type of Request being Requested</param>
        /// <param name="requestedResourceId">The requested resource identifier.</param>
        /// <param name="projectNum">Project associated with the request</param>
        /// <param name="globalResourceRequest">if set to <c>true</c> [global resource request].</param>
        protected internal InRequest(RequestMode requestType, ulong requestedResourceId = InvalidResourceId, ulong projectNum = InvalidProjectNum, bool globalResourceRequest = true) : base(requestType, requestedResourceId, projectNum, globalResourceRequest)
        {
            if ((requestType & RequestMode.WriteRequest) == RequestMode.WriteRequest)
            {
                throw new ArgumentException("InRequest can not be used for writting resources", nameof(requestType));
            }
        }
    }
}