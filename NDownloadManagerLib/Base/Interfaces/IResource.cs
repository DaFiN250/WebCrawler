﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDownloadManagerLib.Base
{
    public interface IResource
    {
        /// <summary>
        /// ResourceId property
        /// This identifies the Resource..
        /// </summary>
        ulong ResourceId { get; set; }
        
        /// <summary>
        /// IsAvailable property
        /// This implies the resource is available for usage, and even if might be a streaming resource, it has been completely made available offline..
        /// </summary>
        bool IsAvailable { get; set; }

        /// <summary>
        /// Name property
        /// A name for the resource.. if any..
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Target property
        /// The real resource link..
        /// </summary>
        string Target { get; set; }

        /// <summary>
        /// GetResource method
        /// Makes resource available.. (makes resource available offline / for usage..)
        /// </summary>
        /// <returns>Returns <c>true</c> if successful, else, returns <c>false</c>.</returns>
        bool GetResource();

        //TODO Complete this..
        /// <summary>
        /// SendResource method
        /// Sends the resource to the Streaming Location.. (makes resource available at an online location.)
        /// <br/>For an instance, this could mean the resource is uploaded to the internet..
        /// </summary>
        /// <returns>Returns <c>true</c> if successful, else, returns <c>false</c>.</returns>
        bool SendResource();
    }
}
