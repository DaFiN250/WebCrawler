﻿using System;
using NDownloadManagerLib.Base.Enums;

namespace NDownloadManagerLib.Base.Interfaces
{
    /// <summary>
    /// Abstract Class Request.
    /// Inheritance: InRequest
    /// </summary>
    public abstract class Request : IComparable<Request>
    {
        /// <summary>
        /// Invalid Request Number
        /// </summary>
        public const ulong InvalidRequestNum = 0;

        /// <summary>
        /// Gets or sets the parent. Owner of the Request
        /// </summary>
        /// <value>The parent</value>
        public object Parent { get; protected internal set; }

        /// <summary>
        /// Invalid Project Number
        /// Incase of global request / resource, this maybe used.
        /// </summary>
        public const ulong InvalidProjectNum = 0;

        /// <summary>
        /// Invalid Resource Identifier
        /// </summary>
        public const ulong InvalidResourceId = 0;

        /// <summary>
        /// Gets or sets the request number.
        /// </summary>
        /// <value>The request number.</value>
        public ulong RequestNum { get; protected internal set; }

        /// <summary>
        /// Gets or sets the project number.
        /// </summary>
        /// <value>The project number.</value>
        public ulong ProjectNum { get; protected set; }

        /// <summary>
        /// Gets or sets the type of the request.
        /// </summary>
        /// <value>The type of the request.</value>
        public RequestMode RequestType { get; protected set; }

        /// <summary>
        /// Gets or sets the request resource identifier.
        /// </summary>
        /// <value>The request resource identifier.</value>
        public ulong RequestResourceId { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is global resource request.
        /// </summary>
        /// <value><c>true</c> if this request instance is for a global resource request; otherwise, <c>false</c>.</value>
        public bool IsGlobalResourceRequest { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is completed.
        /// </summary>
        /// <value><c>true</c> if this request instance is completed; otherwise, <c>false</c>.</value>
        public bool IsCompleted { get; protected set; }

        /// <summary>
        /// Gets or sets the request priority.
        /// </summary>
        /// <value>Lower value = Higher Priority</value>
        public ulong RequestPriority { get; protected set; }

        /// <summary>
        /// Occurs when Request Completed.
        /// Typically <see cref="SetCompleted"/> triggers it.
        /// </summary>
        protected internal event EventHandler Completed;

        /// <summary>
        /// Initializes a new instance of the <see cref="Request"/> class.
        /// </summary>
        /// <param name="requestType">Type of Request being Requested</param>
        /// <param name="requestedResourceId">The requested resource identifier.</param>
        /// <param name="projectNum">The project number associated with the project. <c><see cref="InvalidProjectNum"/></c> in case of Global.</param>
        /// <param name="globalResourceRequest">if set to <c>true</c> [global resource request].</param>
        protected Request(RequestMode requestType, ulong requestedResourceId = Resource.InvalidResourceId, ulong projectNum = InvalidProjectNum,
            bool globalResourceRequest = false)
        {
            if ((globalResourceRequest && projectNum != InvalidProjectNum)
             ||   ((requestType & RequestMode.Project) == RequestMode.Project 
                && (requestType & RequestMode.Global) == RequestMode.Global))
            {
                // check if its a Resource
                if ((requestType & RequestMode.Resource) != RequestMode.Resource)
                    throw new ArgumentException("Request can not be both Global and Project based", nameof(requestType));
            }

            RequestType = requestType;
            RequestResourceId = requestedResourceId;
            ProjectNum = projectNum;
            IsGlobalResourceRequest = globalResourceRequest;
        }

        /// <summary>
        /// Sets the request instance to completed.
        /// </summary>
        public void SetCompleted()
        {
            IsCompleted = true;
            OnCompleted();
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.</returns>
        public int CompareTo(Request other)
        {
            if (other == null)
                return 0;

            if (RequestType.Equals(other.RequestType))
            {
                var priorities = RequestPriority.CompareTo(other);
                return priorities;
            }

            return 0;
        }

        /// <summary>
        /// Equalses the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns><c>true</c> if equals, <c>false</c> otherwise.</returns>
        protected bool Equals(Request other)
        {
            return RequestNum == other.RequestNum && RequestPriority == other.RequestPriority && RequestResourceId == other.RequestResourceId && RequestType == other.RequestType;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns><c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Request) obj);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = RequestNum.GetHashCode();
                hashCode = (hashCode*397) ^ RequestPriority.GetHashCode();
                hashCode = (hashCode*397) ^ RequestResourceId.GetHashCode();
                hashCode = (hashCode*397) ^ (int) RequestType;
                return hashCode;
            }
        }

        /// <summary>
        /// Called when [completed].
        /// </summary>
        /// TODO Edit XML Comment Template for OnCompleted
        protected virtual void OnCompleted()
        {
            Completed?.Invoke(this, EventArgs.Empty);
        }
    }
}