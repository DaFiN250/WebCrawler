﻿using System;
using System.Threading;

namespace NDownloadManagerLib.Base
{
    /// <summary>87
    /// Shows Progress in Console
    /// </summary>
    public class ConsoleProgress
    {
        private Thread Background;

        public ConsoleProgress()
        {

        }

        /// <summary>
        /// ShowProgress of the Download or Process
        /// </summary>
        /// <param name="current"></param>
        /// <param name="max"></param>
        public static void ShowProgress(int current, int max)
        {

            char[] cstr = new char[11];
            cstr[10] = '\0';

            int fill = (current * 10) / max;
            for (int i = 0; i < 10; i++)
            {
                cstr[i] = (fill <= i) ? ' ' : '█';
                //System.out.Write("%c", (i > (current / 10) ? 254, ' '));

                /*int progress = (current * 100) / max;
                if (i == 3 && progress == 100)
                    cstr[i] = '1';
                else if (i == 4)
                    cstr[i] = (char)('0' + ((progress / 10) % 10));
                else if (i == 5)
                    cstr[i] = (char)('0' + (progress % 10));
                else if (i == 6)
                    cstr[i] = (char)'%';*/
            }

            int progress = (current * 100) / max;
            string strProgress = new string(cstr);
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write("[{0:S}] {1:D}%", strProgress, progress);
        }

    }
}
