﻿using NDownloadManagerLib.Base.Enums;
using NDownloadManagerLib.Base.Interfaces;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
namespace NDownloadManagerLib.Base.Net
{
    /// <summary>
    /// Downloads a Resource from the Internet or Intranet
    /// </summary>
    public class DownloadResource
    {
        //TODO add Resource class..
        //TODO add multithread support..
        //TODO add Thread Safe Methods..
        //TODO add ProjectNum and Project support..

        public TransferFileSize DownloadFileSize { get; set; } = TransferFileSize.Megabyte;

        public event DownloadProgressChangedEventHandler UpdateProgress = null;
        //delegate void UpdateProgress(object sender, object args);

        internal WebClient webClient { get; private set; } = null;

        /// <summary>
        /// Download a file from the internet with single thread ..
        /// </summary>
        /// <param name="url">Url to download..</param>
        /// <param name="filename">filename to download the resource to..</param>
        public static DownloadResource CreateDownload(string url, string filename)
        {
            DownloadResource resource = new DownloadResource();

            // TODO threaded download..
            var webClient = resource.webClient = new WebClient();
            var dataSize = webClient.ResponseHeaders.Get(HttpResponseHeader.ContentLength.ToString());
            long fileSize = 0L;
            if (long.TryParse(dataSize.ToString(), out fileSize))
            {
                // success..
                Debug.WriteLine("Content-Length: {0}", dataSize.ToString());
                Debug.WriteLine("File Size: {0}", fileSize.ToString());
            }

            var data = webClient.DownloadData(url);
            // TODO Add Progress record..
            // webClient.DownloadProgressChanged += delegate { UpdateProgress };

            if (resource.UpdateProgress != null)
                resource.webClient.DownloadProgressChanged += resource.UpdateProgress;

            var fname = @"F:\index.html";

            if (string.IsNullOrEmpty(filename) && !string.IsNullOrEmpty(fname))
                filename = fname;

            var file = new File();

            // check if file exists and is of same length
            if (!file.Exists(filename, fileSize))
                file.WriteFile(filename, data);
            
            Console.WriteLine("Writting file : {0}", filename);

            return resource;
        }


        /// <summary>
        /// static function to Download file single file?
        /// </summary>
        /// <param name="url">Url website Url</param>
        /// <param name="filename">filename to download to..</param>
        public void Download(string url, string filename)
        {
            var webClient = new WebClient();
        }

        internal void UpdateConsoleProgress(object sender, DownloadProgressChangedEventArgs args)
        {
            ConsoleProgress.ShowProgress(args.ProgressPercentage, 100);
        }

        /// <summary>
        /// DownloadSingle method
        /// </summary>
        /// <param name="url">Url to download..</param>
        /// <param name="filename">Filename to download to..</param>
        public static void DownloadSingle(string url, string filename)
        {
            DownloadResource downloadResource = new DownloadResource();
            downloadResource.DownloadThreadPart(url, filename);
        }

        /// <summary>
        /// DownloadThreadPart method
        /// </summary>
        /// <param name="url"></param>
        /// <param name="filename"></param>
        /// <param name="startByte"></param>
        /// <param name="endByte"></param>
        public void DownloadThreadPart(string url, string filename, long startByte = 0L, long endByte = 0L)
        {
            //Thread thread = new Thread()
            var webClient = this.webClient;
            if (webClient is null)
            {
                webClient = new WebClient();
            }

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.MaximumAutomaticRedirections = 0;
            webRequest.MaximumResponseHeadersLength = 4;
            webRequest.UseDefaultCredentials = true;
            webRequest.Credentials = CredentialCache.DefaultCredentials;

            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

            var length = webResponse.ContentLength;
            var contentType = webResponse.ContentType;

            webClient.Headers.Add(HttpRequestHeader.Range, "0-0");

            var data = webClient.DownloadData(url);

            var contentRange = (new ContentRangeHeaderValue(startByte, endByte)).ToString();
            var range = (new RangeHeaderValue(startByte, endByte)).ToString();
            Debug.WriteLine("Range={0}", range);
            Debug.WriteLine("Content-Range={0}", contentRange);

            webClient.Headers.Add(HttpRequestHeader.Range, range);
            webClient.Headers.Add(HttpRequestHeader.ContentRange, contentRange);

            // TODO Add Progress record..
            // webClient.DownloadProgressChanged += delegate { UpdateProgress };

            if (this.UpdateProgress != null)
                webClient.DownloadProgressChanged += this.UpdateProgress;

            var fname = @"F:\index.html";

            if (string.IsNullOrEmpty(filename) && !string.IsNullOrEmpty(fname))
                filename = fname;

            File file = new File();
            file.WriteFile(filename, data);

            Console.WriteLine("Writting file : {0}", filename);
        }

        /// <summary>
        /// DownloadPart method
        /// </summary>
        /// <param name="url">The Download URL</param>
        /// <param name="filename">Filename to which the downloaded url is to be saved..</param>
        /// <param name="startByte">Index from which download must start of the File</param>
        /// <param name="length">length of download in bytes</param>
        /// <param name="endByte">Index of File uptill which to download</param>
        public static void DownloadPart(string url, string filename, long startByte, long length, long endByte = 0)
        {
            WebClient webClient = new WebClient();
            HttpClient httpClient = new HttpClient();

            httpClient.BaseAddress = new Uri(url);

            httpClient.DefaultRequestHeaders.UserAgent.Add(new System.Net.Http.Headers.ProductInfoHeaderValue("NDownloadManager"));
            //TODO find all headers usable for part downloading in multipart..
            if (endByte != 0 && length == 0) // length was not specified but endByte was specified.
                httpClient.DefaultRequestHeaders.Range = new RangeHeaderValue(startByte, endByte);
            else // length was specified and endByte was not specified..
            {
                httpClient.DefaultRequestHeaders.Range = new RangeHeaderValue(startByte, startByte + length);
            }

            // TODO Complete this..
        }
    }
}
