﻿using NDownloadManagerLib.Base.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NDownloadManagerLib.Base.Net
{
    public class StreamingResource : Resource
    {
        public Uri StreamingUri { get; set; }

    }
}
