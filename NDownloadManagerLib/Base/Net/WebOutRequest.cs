﻿// ***********************************************************************
// Assembly         : NDownloadManagerLib
// Author           : Anshul
// Created          : 07-30-2023
//
// Last Modified By : Anshul
// Last Modified On : 07-30-2023
// ***********************************************************************
// <copyright file="WebOutRequest.cs" company="">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NDownloadManagerLib.Base.Net
{
    /// <summary>
    /// Class WebOutRequest : Outgoing WebRequests .. Goes Out ->
    /// </summary>
    internal class WebOutRequest : IDisposable
    {
        /// <summary>
        /// The web client instance of underlying client for connectivity
        /// </summary>
        private WebClient _webClient = null;

        /// <summary>
        /// Gets or sets the web client.
        /// </summary>
        /// <value>The web client.</value>
        public WebClient WebClient { get => _webClient; protected set => _webClient = value; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:NDownloadManagerLib.Base.Net.WebOutRequest" /> class.
        /// </summary>
        /// <param name="_client">The client.</param>
        public WebOutRequest(WebClient _client)
        {
            _webClient = _client;
        }

        /// <summary>
        /// Disposes this instance.
        /// </summary>
        public void Dispose()
        {
            ((IDisposable)_webClient).Dispose();
        }
    }
}
