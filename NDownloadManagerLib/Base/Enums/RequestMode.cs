﻿using System;
using System.ComponentModel;

namespace NDownloadManagerLib.Base.Enums
{
    /// <summary>
    /// Enum RequestMode
    /// </summary>
    [Flags]
    public enum RequestMode : int
    {
        /// <summary>
        /// Unknown Request Mode or Invalid Request
        /// </summary>
        [Description("Invalid / Unknown")]        
        Unknown = 0,

        /// <summary>
        /// Request is for Reading Resource
        /// </summary>
        [Description("Request is for Reading Resource")]
        ReadRequest = 1 << 0,

        /// <summary>
        /// Request is for Writting Resource
        /// </summary>
        [Description("Request is for Writting Resource")]
        WriteRequest = 1 << 1,

        /// <summary>
        /// Request is Global Resource (Non restrictive)
        /// </summary>
        [Description("Request is Global Resource (Non restrictive)")]
        Global = 1 << 2,

        /// <summary>
        /// Request is a Project based resource (Restrictive)
        /// </summary>
        [Description("Request is a Project based resource (Restrictive)")]
        Project = 1 << 3,

        [Description("Its A Resource")]
        Resource = 1 << 7
    }
}