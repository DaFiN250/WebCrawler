﻿using System.ComponentModel;

namespace NDownloadManagerLib.Base.Enums
{
    /// <summary>
    /// Enum LockState
    /// Indicates Resource Lock State
    /// </summary>
    public enum LockState
    {
        /// <summary>
        /// Resource not Locked
        /// </summary>
        [Description("Resource not Locked")]
        UnLocked = 0x00,

        /// <summary>
        /// Resource Locked
        /// </summary>
        [Description("Resource Locked")]
        Locked = 0x01
    }
}