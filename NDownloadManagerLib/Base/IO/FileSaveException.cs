﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDownloadManagerLib.Base.IO
{
    public class FileSaveException : Exception
    {
        public string FileName { get; internal set; } = string.Empty;

        public FileSaveException() : base(@"Unable to save file") { }
        
        public FileSaveException(string message) : base(message)
        {
        }

        public FileSaveException(string message, string filename) : base(message)
        {
            FileName = filename;
        }
    }
}
