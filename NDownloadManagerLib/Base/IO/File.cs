﻿using NDownloadManagerLib.Base.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NDownloadManagerLib.Base.IO
{
    public class File : Resource
    {
        private readonly object _lockFragments = new object();
        public List<Fragment> Fragments { get; set; } = null;

        /// <summary>
        /// FileSize property
        /// </summary>
        public long FileSize { get; set; } = -1L;

        /// <summary>
        /// Filename property
        /// </summary>
        public string Filename { get; set; }

        private string _url = string.Empty;

        /// <summary>
        /// Url property
        /// </summary>
        public string Url
        {
            get => _url;
            set
            {
                _url = value;
                if (!string.IsNullOrEmpty(_url)) {
                    IsStreaming = true;

                    if (!string.IsNullOrEmpty(Filename))
                    {
                        Uri uri = new Uri(value);
                        Filename = System.IO.Path.GetFileName(uri.LocalPath);
                    }
                }
            }
        }

        /// <summary>
        /// IsComplete property
        /// </summary>
        public bool IsComplete { get; internal set; } = false;

        /// <summary>
        /// IsStreaming property
        /// </summary>
        public bool IsStreaming { get; internal set; } = false;
                                                                                                             
        /// <summary>
        /// File constructor method
        /// </summary>
        public File() { }

        /// <summary>
        /// File constructor method
        /// </summary>
        /// <param name="filename"><see cref="Filename"/> property is filled up with this filename parameter.</param>
        public File(string filename) { Filename = filename; }

        /// <summary>
        /// Write method
        /// Writes the <see cref="Fragment"/> to this <see cref="File"/> in disk.
        /// </summary>
        /// <param name="fragment"><see cref="Fragment"/> of this <see cref="File"/></param>
        public void Write(Fragment fragment) 
        { 
            using (FileStream fs = new FileStream(Filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                // TODO Allocate space for file..
                long newPosition = fs.Seek(fragment.StartByte - 1, SeekOrigin.Begin);
                if (newPosition == fragment.StartByte)
                {
                    // Write fragment to file..
                    // TODO Complete this..
                }
            }
        }

        /// <summary>
        /// Read method
        /// Reads the <see cref="Fragment"/> from this <see cref="File"/> in disk.
        /// </summary>
        /// <param name="fragment"><see cref="Fragment"/> of this <see cref="File"/></param>
        public void Read(Fragment fragment) { }

        /// <summary>
        /// Join method 
        /// Method to join all Fragments to form this File..
        /// </summary>
        public void Join() { }

        public void SortFragments()
        {
            lock (_lockFragments)
            {
                Fragments.Sort();
            }
        }

        /// <summary>
        /// PreAllocate method
        /// Preallocates complete file on the disk..
        /// </summary>
        /// <returns>Returns <c>true</c> if successful, else, returns <c>false</c>.</returns>
        public bool PreAllocate()
        {
            // Another method ..
            // fsutil file createnew <filename> <filesize>

            if (string.IsNullOrEmpty(this.Url)) return false;

            try
            {
                if (string.IsNullOrEmpty(Filename))
                {
                    Uri uri = new Uri(this.Url);
                    this.Filename = Path.GetFileName(uri.LocalPath);
                }

                //TODO add temproray extension for preallocated file..
                //TODO handle temporary extension and rename file in the end..
                if (System.IO.File.Exists(this.Filename))
                {
                    // TODO Add confirmation..
                    System.IO.File.Delete(this.Filename);
                }

                GetFileSize(this.Url);

                FileStream fs = System.IO.File.Open(this.Filename, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.ReadWrite);

                if (FileSize > 0) fs.Seek(FileSize, SeekOrigin.Begin);

                fs.Flush();
                fs.Close();

                return true;
            }
            catch (Exception e)
            {
                // TODO Handle Exceptions to support User Interface..
                Debug.WriteLine(e);
            }

            return false;
        }

        /// <summary>
        /// GetFileSize
        /// </summary>
        /// <param name="url">Use another Url other than that specified in <see cref="Url"/> property</param>
        /// <returns>Returns FileSize and internally sets <see cref="FileSize"/> property</returns>
        public long GetFileSize(string url = null)
        {
            long contentLength = 0;
            HttpWebRequest webRequest = null;
            if (!string.IsNullOrEmpty(url))
                webRequest = HttpWebRequest.CreateHttp(url);
            else
                webRequest = HttpWebRequest.CreateHttp(Url);

            // HEAD request to url or Url property specified Uri

            webRequest.Method = "HEAD";

            HttpWebResponse webResponse = webRequest.GetResponse() as HttpWebResponse;
            if (webResponse != null && (webResponse).StatusCode == HttpStatusCode.OK)
            {
                // find content length and close the connection
                contentLength = webResponse.ContentLength;
                webResponse.Close();
            }
            else
            {
                contentLength = -1L;
                webResponse?.Close();
            }
            
            if (string.IsNullOrEmpty(url))
                FileSize = contentLength;

            return contentLength;
        }

        public Fragment CreateFragment()
        {
            Fragment splitFromFragment = null;
            Fragment newFragment = null;

            if (FileSize < 0)
            {
                this.GetFileSize();
            }

            if (Fragments == null)
            {
                Fragments = new List<Fragment>()
                {
                        (newFragment = new Fragment(this) { StartByte = 1, EndByte = FileSize}),
                };
            }

            else if (Fragments != null)
            {
                // find maxsize of fragment from all fragments.. (Fragments)

                long maxSize = 0;
                Fragments.ForEach(fragment =>
                {
                    if (FileSize != -1 && fragment.EndByte == -1)
                        fragment.EndByte = FileSize;

                    if (fragment.EndByte != -1)
                    {
                        long size = fragment.EndByte - fragment.StartByte + 1;
                        if (size > maxSize)
                        {
                            splitFromFragment = fragment;
                            maxSize = size;
                        }
                    }
                });

                if (maxSize != 0)
                {
                    // and divide it to half..
                    // the remaining half we get new fragment..
                    // TODO how to download new fragment..?? add to Fragment Class..
                    newFragment = new Fragment(this)
                    {
                        StartByte = splitFromFragment.StartByte + maxSize / 2 + (maxSize % 2),
                        EndByte = splitFromFragment.EndByte
                    };

                    lock (_lockFragments)
                    {
                        this.Fragments.Add(newFragment);
                    }

                    if (newFragment.StartByte > 1)
                        splitFromFragment.EndByte = newFragment.StartByte - 1;
                    else if (newFragment.StartByte == 1)
                        splitFromFragment.EndByte = 1;
                }
            }

            return newFragment;
        }

        public override bool GetResource()
        {
            //TODO complete this..
            GetFileSize();
            for (int i = 0; i < Configuration.ConfigurationInstance.NumberOfThreadsPerFile; i++)
                CreateFragment();

            lock (_lockFragments)
            {
                foreach (var fragment in Fragments)
                {
                    fragment.Download();
                }
            }

            return true;
        }
    }
}
