﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NDownloadManagerLib.Base
{
    /// <summary>
    /// File class for Content Type & FileInformation Handling & others..
    /// </summary>
    [DataContract(Name = "File")]
    public class File
    {
        internal FileStream _fileStream;
        internal CancellationTokenSource _cancellationTokenSource = null;

        [DataMember]
        public string Path { get; set; }

        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public string FileExtension { get; set; }

        [DataMember]
        public string FileType { get; set; }

        [DataMember]
        public ulong Size { get; set; }

        [DataMember]
        public DateTime Timestamp { get; set; }

        [DataMember]
        public FileTypeInfo FileTypeInformation { get; set; }

        public File() { }

        /// <summary>
        /// Exists method
        /// </summary>
        /// <returns>Returns <c>true</c> if file exists, else returns <c>false</c>.</returns>
        /// <exception cref="ArgumentNullException">if <see cref="FileName">Filename</see> is not defined, throw exception.</exception>
        public bool Exists()
        {
            if (string.IsNullOrEmpty(FileName))
                throw new ArgumentNullException(nameof(FileName));

            return System.IO.File.Exists(FileName);
        }

        /// <summary>
        /// Exists method
        /// </summary>
        /// <param name="fileName">fileName to verify if exists or not</param>
        /// <returns>Returns <c>true</c> if file exists, else returns <c>false</c>.</returns>
        /// <exception cref="ArgumentNullException">if argument fileName is not defined throw exception.</exception>
        public bool Exists(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new ArgumentNullException(nameof(FileName));

            return System.IO.File.Exists(fileName);
        }

        public long GetFileSize(string fileName)
        {
            FileInfo fileInfo = new FileInfo(fileName);

            return fileInfo.Length;
        }

        /// <summary>
        /// Exists method
        /// </summary>
        /// <param name="fileName">fileName to verify if exists or not</param>
        /// <param name="length">The length of the file to be expected, if same as file then it returns true on Exists method</param>
        /// <returns>Returns <c>true</c> if file exists, else returns <c>false</c>.</returns>
        /// <exception cref="ArgumentNullException">if argument fileName is not defined throw exception.</exception>
        public bool Exists(string fileName, long length)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new ArgumentNullException(nameof(FileName));

            return System.IO.File.Exists(fileName) && length == GetFileSize(fileName);
        }

        /// <summary>
        /// WriteFile method
        /// </summary>
        /// <param name="filename">Filename of the File</param>
        /// <param name="dataBytes">Data to be written in <see cref="byte"/></param>
        /// <returns><see cref="ulong"/> data bytes read count is returned..</returns>
        public ulong WriteFile(string filename, byte []dataBytes)
        {
            ulong fileSize = 0L;

            bool confirm = false;

            if (_cancellationTokenSource != null)
            {
                // TODO already ongoing operations..
                // TODO verify if must cancel or, must not cancel..
                _cancellationTokenSource.Cancel();

                if (System.IO.File.Exists(FileName))
                {
                    // TODO verify if must delete..
                    // TODO verify the filesize and the current downloading filesize..
                    // TODO Delete if must delete..
                    if (confirm)
                    {
                        System.IO.File.Delete(FileName);
                    }
                    else
                    {
                        Debug.WriteLine("Not Deleting existing file \"{0}\"", FileName);
                    }
                }
            }

            _cancellationTokenSource = new CancellationTokenSource();
            if (System.IO.File.Exists(filename))
            {
                // TODO confirm..
                if (confirm)
                { 
                    System.IO.File.Delete(filename);
                }

                else 
                {
                    return ulong.MaxValue;
                }
            }

            _fileStream = System.IO.File.OpenWrite(filename);
            
            Task t = _fileStream.WriteAsync(dataBytes, 0, dataBytes.Length, _cancellationTokenSource.Token);
            t.Start();

            //System.IO.File.WriteAllBytes(filename, dataBytes);

            return fileSize;
        }
    }
}
