﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NDownloadManagerTheme
{
    public partial class NDownloadManagerThemeForm : Form
    {
        /// <summary>
        /// MainForm property implies the Form is a MainForm & on Closing exits the Application..
        /// </summary>
        public bool MainForm { get; set; } = true;

        /// <summary>
        /// ConfirmClose property implies if <c>true</c>, the closing of form will be confirmed by the user, else it will close the form without confirming.
        /// </summary>
        public bool ConfirmClose { get; set; } = true;

        /// <summary>
        /// _isDialogBox Private field..
        /// </summary>
        private bool _isDialogBox = false;

        /// <summary>
        /// DialogBox property, Set DialogResult, Window Border Style??
        /// </summary>
        public bool IsDialogBox
        {
            get => _isDialogBox;
            set
            {
                if (value && DialogResult == DialogResult.None)
                {
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    DialogResult = DialogResult.None;
                    this.FormBorderStyle = FormBorderStyle.FixedDialog;
                }

                _isDialogBox = value;
            }
        }

        /// <summary>
        /// Constructor Method
        /// </summary>
        public NDownloadManagerThemeForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Close Button .. Click() method
        /// </summary>
        /// <param name="sender"><see cref="object"/> caller to the this Control.Click() method / event</param>
        /// <param name="e"><see cref="EventArgs"/></param>
        private void buttonThemeClose_Click(object sender, EventArgs e)
        {
            if (this.MainForm)
            {
                Application.Exit();
            }
            else
            {
                this.Close();
            }
        } 

        /// <summary>
        /// OnFormClosing() method
        /// </summary>
        /// <param name="sender"><see cref="object"/> caller to the this Control.Click() method / event</param>
        /// <param name="e"><see cref="FormClosingEventArgs"/></param>
        private void NDownloadManagerThemeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ConfirmClose)
            {
                DialogResult result = MessageBox.Show("Close This?", "Close This?", MessageBoxButtons.YesNo);

                if (result != DialogResult.Yes)
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
