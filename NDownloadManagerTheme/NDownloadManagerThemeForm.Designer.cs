﻿namespace NDownloadManagerTheme
{
    partial class NDownloadManagerThemeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NDownloadManagerThemeForm));
            this.buttonThemeClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonThemeClose
            // 
            resources.ApplyResources(this.buttonThemeClose, "buttonThemeClose");
            this.buttonThemeClose.BackColor = System.Drawing.Color.Red;
            this.buttonThemeClose.ForeColor = System.Drawing.Color.White;
            this.buttonThemeClose.Name = "buttonThemeClose";
            this.buttonThemeClose.UseVisualStyleBackColor = false;
            this.buttonThemeClose.Click += new System.EventHandler(this.buttonThemeClose_Click);
            // 
            // NDownloadManagerThemeForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ControlBox = false;
            this.Controls.Add(this.buttonThemeClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "NDownloadManagerThemeForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NDownloadManagerThemeForm_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonThemeClose;
    }
}

