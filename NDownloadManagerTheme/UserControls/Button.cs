﻿using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDownloadManagerTheme.UserControls
{
    public partial class Button : UserControl
    {
        public Button()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Button_Paint method
        /// </summary>
        /// <param name="sender"><see cref="object"/></param>
        /// <param name="e"><see cref="PaintEventArgs"/></param>
        private void Button_Paint(object sender, PaintEventArgs e)
        {
            SetBackground();
        }

        /// <summary>
        /// SetBackground method
        /// </summary>
        private void SetBackground()
        {
            flowLayoutPanelBackground.SuspendLayout();
            flowLayoutPanelBackground.BackColor = Color.White;

            var graphics = this.CreateGraphics();

            var borderPen = (Pen)Pens.DarkGray.Clone();
            borderPen.Width = 2;

            var backgroundBrush = (Brush)Brushes.Blue.Clone();

            graphics.DrawRectangle(borderPen, 0, 0, this.Width, this.Height);
            graphics.FillRectangle(backgroundBrush, 0, 0, this.Width, this.Height);

            flowLayoutPanelBackground.ResumeLayout(true);
        }
    }
}