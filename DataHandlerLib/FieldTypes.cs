﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorganiteVerifier.Core
{
    // TODO add remaining field list later..
    // TODO rename fields later..
    public enum FieldType : uint
    {
        Unknown = 0,

        [Description("StationCode")]
        StationCode = 1,

        [Description("TransactionDate")]
        TransactionDate = 2,

        [Description("TransactionTime")]
        TransactionTime = 3,

        [Description("RejectCode")]
        RejectCode = 4,

        [Description("Entry")]
        //EntryBit = 5,
        Entry = 5,

        [Description("JourneyManagement")]
        //JourneyCount = 6,
        JourneyManagement = 6,

        [Description("FareTier")]
        FareTier = 7,

        [Description("TokenAmount")]
        TokenAmount = 8
    }
}
