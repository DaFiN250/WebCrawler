﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganiteVerifier.Core
{
    public class MorganDataFieldPermission
    {
        public static readonly List<MorganDataFieldPermission> Presets = new List<MorganDataFieldPermission>()
        {
            new MorganDataFieldPermission(false, false),
            new MorganDataFieldPermission(true, false),
            new MorganDataFieldPermission(false, true),
            new MorganDataFieldPermission(true, true)
        };

        public enum Preset : int
        {
            Forbidden = 0,
            ReadOnly = 1,
            WriteOnly = 2,
            ReadWrite = 3
        }

        public bool Read  { get; protected set; }
        public bool Write { get; protected set; }

        public int Value => ((Read? 1 : 0) << 1) | (Write? 1 : 0);

        public MorganDataFieldPermission(bool read, bool write)
        {
            Read = read;
            Write = write;
        }

        public static implicit operator MorganDataFieldPermission(Preset select)
        {
            return Presets[(int)select];
        }

        public MorganDataFieldPermission(string permissions)
        {
            permissions.Trim();
            var len = permissions.Length;
            if (len >= 2)
            {
                Write = permissions.Substring(2, 1).Equals("Y", StringComparison.OrdinalIgnoreCase);
            }

            if (len >= 2)
            {
                Read = permissions.Substring(1, 1).Equals("Y", StringComparison.OrdinalIgnoreCase);
            }
        }

        public override bool Equals(object obj)
        {
			return obj is MorganDataFieldPermission && this.Value == ((MorganDataFieldPermission)obj).Value;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override string ToString()
        {
            return (Read ? "Y" : "N") + (Write ? "Y" : "N");
        }
    }
}