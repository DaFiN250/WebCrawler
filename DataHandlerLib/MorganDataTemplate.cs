﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using LibCSCReader;

using ToolsLib;

namespace MorganiteVerifier.Core
{
	public class MorganDataTemplate : IEnumerable<MorganDataField>
	{
        protected int curfieldOffset = 0;
        public int MorganDataSize => curfieldOffset;

        public bool PopulatedMorganDataFields { get; protected set; } = false;

        protected bool isSorted = false;

        //fieldName, field
        protected Dictionary<int, MorganDataField> dataFieldDictionary = null;

		public Dictionary<int, MorganDataField> DataFieldDictionary
		{
			get
			{
				//if ((dataFieldDictionary?.Count ?? 0) == 0)

				if (dataFieldDictionary == null)
				{
					dataFieldDictionary = new Dictionary<int, MorganDataField>();
				}

				return dataFieldDictionary;
			}
		}

        protected List<MorganDataField> customizableFieldList = null;

        public List<MorganDataField> CustomizableFieldList
        {
            get
            {
                //if ( (dataFieldDictionary?.Count ?? 0) == 0 || customizableFieldList == null)

                if (customizableFieldList == null)
                {
                    customizableFieldList = new List<MorganDataField>();
                }

                return customizableFieldList;
            }
        }

        public void SetPopulated()
        {
            Sort(true);
            PopulatedMorganDataFields = true;
        }

        public delegate void MorganDataEventHandler(MorganDataEventArgs morganDataEventArgs);

		protected event MorganDataEventHandler onPopulateFields;

		public event MorganDataEventHandler OnPopulateFields
		{
			add { onPopulateFields += value; }
			remove { onPopulateFields -= value; }
		}

		public bool PopulateFields(bool force = false)
		{
            bool isValid = false;

            if (force && onPopulateFields != null)
            {
                dataFieldDictionary?.Clear();
                customizableFieldList?.Clear();
                PopulatedMorganDataFields = false;               
            }

            if (dataFieldDictionary == null)
                dataFieldDictionary = new Dictionary<int, MorganDataField>();

            if (customizableFieldList == null)
                customizableFieldList = new List<MorganDataField>();

            bool populatedMorganDataFields = PopulatedMorganDataFields;
            if (onPopulateFields != null)
			{
				var eventArgs = new MorganDataEventArgs(this, ref dataFieldDictionary);

				onPopulateFields.Invoke(eventArgs);

				// it would be better to have it automated..
				SetPopulated();
                isValid = true;
			}

			// Before Invoke !populated
			// After  Invoke populated
			if (!populatedMorganDataFields && PopulatedMorganDataFields)
			{
                // Sort field list (not required)
                // Only required in case of CreateWithOffset
                Sort();
                isValid = true;
			}

            return isValid;
		}

        public MorganDataTemplate(IEnumerable<MorganDataField> dataFieldList = null)
        {
            dataFieldDictionary = new Dictionary<int, MorganDataField>();

            if (dataFieldList != null)
            {
                var sortedDataFieldList = dataFieldList.OrderBy(item => item.FieldOffset);
                foreach (var item in sortedDataFieldList)
                {
                    if (dataFieldDictionary.ContainsKey(item.FieldOffset))
                        throw new ArgumentException("Duplicate FieldOffsets in DataFieldList", nameof(dataFieldList));

                    dataFieldDictionary.Add(item.FieldOffset, item);
                    curfieldOffset += item.FieldSize;
                }

                SetPopulated();
            }
        }

		public int IndexOf(MorganDataField item)
		{
			int index = -1, curIndex = -1;
			PopulateFields();

			if (dataFieldDictionary == null)
			{ return -1; }

			foreach (var kp in dataFieldDictionary)
			{
				++curIndex;
				if (kp.Value == item)
				{
					index = curIndex;
					break;
				}
			}

			return index;
		}

		public void Add(MorganDataField item)
		{
			if (!DataFieldDictionary.ContainsValue(item))
            {
                dataFieldDictionary.Add(item.FieldOffset, item);
                curfieldOffset += item.FieldSize;

                isSorted = false;
            }
		}

        public MorganDataField CreateMorganDataField(string fieldName,
                                                        int fieldSize,
                                                        string fieldDescription,
                                                        MorganDataFieldPermission.Preset fieldPermission = MorganDataFieldPermission.Preset.ReadWrite,
                                                        ConvertibleObject fieldValue = null)
        {
            if (fieldSize < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(fieldSize));
            }

            if (dataFieldDictionary == null)
            {
                dataFieldDictionary = new Dictionary<int, MorganDataField>();
            }

            if (dataFieldDictionary.ContainsKey(curfieldOffset))
            {
                throw new ArgumentException("Duplicate FieldOffset .. Revalidate Offsets or Use CreateMorganDataFieldWithOffset");
            }

            MorganDataField dataField = MorganDataField.Create(this, fieldName, fieldSize, fieldDescription, fieldPermission, fieldValue);
            //dataFieldDictionary.Add(dataField.FieldOffset, dataField);

            return dataField;
        }

        public MorganDataField CreateMorganDataFieldWithOffset(int fieldOffset,
                                                        string fieldName,
                                                        int fieldSize,
                                                        string fieldDescription,
                                                        MorganDataFieldPermission.Preset fieldPermission = MorganDataFieldPermission.Preset.ReadWrite,
                                                        ConvertibleObject fieldValue = null)
        {
            if (fieldOffset < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(fieldOffset));
            }

            if (fieldSize < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(fieldSize));
            }

            if (dataFieldDictionary == null)
            {
                dataFieldDictionary = new Dictionary<int, MorganDataField>();
            }

            if (dataFieldDictionary.ContainsKey(fieldOffset))
            {
                throw new ArgumentException("Duplicate FieldOffset ..", nameof(fieldOffset));
            }

            MorganDataField dataField = MorganDataField.CreateWithOffset(this, fieldOffset, fieldName, fieldSize, fieldDescription, fieldPermission, fieldValue);
            //dataFieldDictionary.Add(dataField.FieldOffset, dataField);

            return dataField;
        }
        protected void Sort(bool force = false)
        {
            if (!isSorted || force)
            {
                dataFieldDictionary = DataFieldDictionary.OrderBy(kp => kp.Key).ToDictionary(kp => kp.Key, kp => kp.Value);

                // Fill CustomizableFieldList
                customizableFieldList = DataFieldDictionary.Values.Where(field => field.CustomizableOnCreate).ToList();
            }
        }

        public bool Contains(MorganDataField item)
		{
            return DataFieldDictionary.ContainsValue(item);
		}

		public void CopyTo(MorganDataField[] array, int arrayIndex)
		{
            DataFieldDictionary.Values.CopyTo(array, arrayIndex);
		}

		public bool Remove(MorganDataField item)
		{
            return DataFieldDictionary.Remove(item.FieldOffset);
		}

		public IEnumerator<MorganDataField> GetEnumerator()
		{
            return DataFieldDictionary.Values.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
            return GetEnumerator();
        }
		
		public int Count => DataFieldDictionary.Count;

		public bool IsReadOnly => false;

		public MorganDataField this[int index]
		{
			get { return DataFieldDictionary.Values.ElementAt(index); }
        }
    }
}
