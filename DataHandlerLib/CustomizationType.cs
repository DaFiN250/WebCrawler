﻿using System;
namespace MorganiteVerifier.Core
{
	public enum CustomizationType : int
	{
		DOSDATE = 0,
		DOSTIME = 1,
		BYTE = 2,
		INT16 = 3,
		UINT16 = 4,
		INT32 = 5,
		UINT32 = 6,
		CONVOBJ = 7,
		LIST = 8
	}
}
