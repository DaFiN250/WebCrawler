﻿using LibCSCReader.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorganiteVerifier.Core
{
    public class InstructionSetPreset
    {
        public int TicketType { get; private set; }

        public DEST_TYPE DestCardType { get; set; }

        public CSC_TYPE SpecificationType { get; private set; }

        public Dictionary<InstructionType, InstructionSet> Preset { get; set; }

        public InstructionSetPreset(int ticketType, CSC_TYPE specificationType, Dictionary<InstructionType, InstructionSet> preset, DEST_TYPE destType = DEST_TYPE.DEST_CARD)
        {
            TicketType = ticketType;
            SpecificationType = specificationType;
            Preset = preset;
            DestCardType = destType;
        }

        public void UpdatePreset(InstructionType instructionType, InstructionSet instructionSet)
        {
            if (!Preset.ContainsKey(instructionType))
            {
                Preset.Add(instructionType, null);
            }

            Preset[instructionType] = instructionSet;
        }
    }
}
